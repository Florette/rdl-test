const state = {
    type: '',
    size: {
        width: '',
        height: '',
    }
};
const getters = {
    GET_VIEWPORT: state => state // отдаю viewport
};
const mutations = {
    SET_VIEWPORT: (state, payload) => { // принимаю данные в хранилище
        state.size.width = payload.width;
        state.size.height = payload.height;
        if (payload.width >= 1 && payload.width <= 767) {
            state.type = 'mobile'
        } else if (payload.width >= 768) {
            state.type = 'desktop'
        }
    }
};
const actions = {};

export default {
    state,
    getters,
    mutations,
    actions,
};