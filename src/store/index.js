import Vue from 'vue';
import Vuex from 'vuex';
import viewport from './viewport'; // импортию модуль хранящий viewport

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        viewport // инициализация
    }
});
