import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.mixin({
    computed: {
        $viewport() {
            return this.$store.getters.GET_VIEWPORT; // делаю видимой глобально через миксин
        }
    }
});

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
